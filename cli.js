#!/usr/bin/env node
'use strict';

const markdown_extensions = require('markdown-extensions');
const start = require('unified-args');

const pack = require('./package.json');
const relias = require('./index.js');

const name = pack.name;

start({
    processor: relias(),
    name: name,
    description: pack.description,
    version: pack.version,
    extensions: markdown_extensions,
    packageField: name + 'Config',
    rcName: '.' + name + 'rc',
    ignoreName: '.' + name + 'ignore',
});
