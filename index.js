'use strict';

const unified = require('unified');
const parse = require('remark-parse');
const math = require('remark-math');
const remark2rehype = require('remark-rehype');
const stringify = require('rehype-stringify');
const visit = require('unist-util-visit');

function remove_solution() {
    return function transform(tree) {
        tree.children = tree.children.filter(function(elm) {
            return !(elm.type === 'code' && elm.meta && elm.meta.split(' ')[0] === 'cell')
        })
    }
}

function wrap_math() {
    // Ilias uses the .latex class for math elements, but only on spans, not on
    // divs. Therefore, display math appears to be impossible. It is, however,
    // possible to circumvent Ilias and use MathJax' markup directly. This
    // transformer wraps the math elements (as produced by remark-math) in the
    // corresponding \(..\) or \[..\] pairs. The original span/div does not
    // hurt and is left in place for simplicity.

    return function transform(tree) {
        visit(tree, 'element', function(element) {
            const classes = element.properties.className || [];
            if (classes.includes('math-inline')) {
                element.children[0].value = '\\(' + element.children[0].value + '\\)';
            } else if (classes.includes('math-display')) {
                element.children[0].value = '\\[' + element.children[0].value + '\\]';
            }
        });
    }
}

module.exports = unified()
    .use(parse)
    .use(remove_solution)
    .use(math)
    .use(remark2rehype)
    .use(wrap_math)
    .use(stringify)
    .freeze();
